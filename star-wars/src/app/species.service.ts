import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
//https://swapi.dev/api/species


export class SpeciesService { //https://swapi.dev/api/planets/
  species!:any
  constructor(private http:HttpClient) { }

  public getSpecies():Observable<any> {
    return this.http.get<any>(`https://swapi.dev/api/species`);
  }
}
