import { Component,Input ,OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { SpeciesService } from '../species.service';

@Component({
  selector: 'app-user3',
  templateUrl: './user3.component.html',
  styleUrls: ['./user3.component.css']
})
export class User3Component implements OnInit {

  product:any

  user3:any
  constructor(private speciesService:SpeciesService, private activatedroute:ActivatedRoute,  private router: Router) {
  }
  ngOnInit(): void {
    this.getSpeciesInfo()

  }

  getSpeciesInfo(){
    let id=this.activatedroute.snapshot.params['id']
    this.speciesService.getSpecies().subscribe(data=>{
  
     this.user3=data.results[id]
     console.log(this.user3)
    })
  }

}
