import { Component, OnInit } from '@angular/core';
import { SpeciesService} from '../species.service'
import {NavigationExtras} from "@angular/router";


@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
  styleUrls: ['./species.component.css']
})
export class SpeciesComponent implements OnInit {
  species?:any
  data:any="kdlksd";
  constructor(private speciesService:SpeciesService) { }

  ngOnInit(): void {
    this.getSpecies();
      }

  getSpecies(){
    this.speciesService.getSpecies().subscribe((data: { results: any; })=>{
      this.species=data.results
  
    })
  }

}