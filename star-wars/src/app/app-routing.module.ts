import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {PeopleComponent} from "./people/people.component";
import {UserComponent} from "./user/user.component";
import { PlantsComponent } from './plants/plants.component';
import { User2Component } from './user2/user2.component';
import { SpeciesComponent } from './species/species.component';
import { User3Component } from './user3/user3.component';
import { StarshipsComponent } from './starships/starships.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { User5Component } from './user5/user5.component';
import { User4Component } from './user4/user4.component';

const routes:Routes=[
  {
    path:'people', component:PeopleComponent
  },
  {
    path:"", component:PeopleComponent
  },
  {
    path:'',
    children:[
      {path:"user/:id", component:UserComponent, pathMatch:'full'}
    ]
  },
  {
    path:'plants', component:PlantsComponent
  },
   { path:"", component:PlantsComponent
  },

  {
    path:'',
    children:[
      {path:"user2/:id", component:User2Component, pathMatch:'full'}
    ]
  }, 
  {
    path:'species', component:SpeciesComponent
  },
  {
    path:"", component:SpeciesComponent
  },
  {
    path:'',
    children:[
      {path:"user3/:id", component:User3Component, pathMatch:'full'}
    ]
  },
  {
    path:'starships', component:StarshipsComponent
  },
  {
    path:"", component:StarshipsComponent
  },
  {
    path:'',
    children:[
      {path:"user4/:id", component:User4Component, pathMatch:'full'}
    ]
  },
  {
    path:'vehicles', component:VehiclesComponent
  },
  {
    path:"", component:VehiclesComponent
  },
  {
    path:'',
    children:[
      {path:"user5/:id", component:User5Component, pathMatch:'full'}
    ]
  }


]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule],
 
})
export class AppRoutingModule { }
