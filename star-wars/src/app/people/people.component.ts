import { Component, OnInit, Output } from '@angular/core';
import { PeoplesService } from '../peoples.service';
import { NavigationExtras } from '@angular/router';
import { People } from 'src/people';

import { switchMap, concatMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css'],
})
export class PeopleComponent implements OnInit {
  peoples?: any;

  constructor(
    private peopleService: PeoplesService,
    private http: HttpClient
  ) {} //read

  ngOnInit(): void {
    this.getPeoples();
  }

  getPeoples() {
    // forkJoin(this.peopleService.getPeoples, this.peopleService.getPeoplesPage2).subscribe((d)=>console.log(d))
  }
}
