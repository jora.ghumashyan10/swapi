import { Component, OnInit } from '@angular/core';
import {StarshipsService} from '../starships.service'
import {NavigationExtras} from "@angular/router";


@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.css']
})
export class StarshipsComponent implements OnInit {
  starships?:any
  data:any="kdlksd";
  constructor(private starshipsService:StarshipsService) { }

  ngOnInit(): void {
    this.getStarships();
      }

  getStarships(){
    this.starshipsService.getStarships().subscribe((data: { results: any; })=>{
      this.starships=data.results
  
    })
  }

}