import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class PlantsService { //https://swapi.dev/api/planets/
  plants!:any
  constructor(private http:HttpClient) { }

  public getPlants():Observable<any> {
    return this.http.get<any>(`https://swapi.dev/api/planets`);
  }
}

