import { Component, OnInit, Output } from '@angular/core';
import { PlantsService } from '../plants.service';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-plants',
  templateUrl: './plants.component.html',
  styleUrls: ['./plants.component.css'],
})
export class PlantsComponent implements OnInit {
  plants?: any;

  constructor(private plantsService: PlantsService) {}

  ngOnInit(): void {
    this.getPlants();
  }

  getPlants() {
    this.plantsService.getPlants().subscribe((data: { results: any }) => {
      this.plants = data.results;
      //  console.log(this.peoples)
    });
  }
}
