import { Component, Input, OnInit } from '@angular/core';
import { PeoplesService } from '../peoples.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  product: any;

  user: any;
  constructor(
    private peopleService: PeoplesService,
    private activatedroute: ActivatedRoute,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.getPeopleInfo();
  }

  getPeopleInfo() {
    let id = this.activatedroute.snapshot.params['id'];
    this.peopleService.getPeoples().subscribe((data) => {
      //  this.user=data.results[id]
      console.log(this.user);
    });
  }
}
