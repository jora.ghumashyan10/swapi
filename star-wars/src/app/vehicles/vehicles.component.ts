import { Component, OnInit } from '@angular/core';
import { VehiclesService} from '../vehicles.service'



@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {
  vehicles?:any
  data:any="kdlksd";
  constructor(private vehiclesService:VehiclesService) { }

  ngOnInit(): void {
    this.getVehicles();
      }

  getVehicles(){
    this.vehiclesService.getVehicles().subscribe((data: { results: any; })=>{
      this.vehicles=data.results
  
    })
  }

}
