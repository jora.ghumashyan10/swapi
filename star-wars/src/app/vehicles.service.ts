import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class VehiclesService { 
  vehicles!:any
  constructor(private http:HttpClient) { }

  public getVehicles():Observable<any> {
    return this.http.get<any>(`https://swapi.dev/api/vehicles`);
  }
}