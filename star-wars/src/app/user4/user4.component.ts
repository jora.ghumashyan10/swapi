import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { StarshipsService } from '../starships.service';

@Component({
  selector: 'app-user4',
  templateUrl: './user4.component.html',
  styleUrls: ['./user4.component.css']
})
export class User4Component implements OnInit {

  product:any

  user4:any
  constructor(private starshipsService:StarshipsService, private activatedroute:ActivatedRoute,  private router: Router) {
  }
  ngOnInit(): void {
    this.getStarshipsInfo()

  }

  getStarshipsInfo(){
    let id=this.activatedroute.snapshot.params['id']
    this.starshipsService.getStarships().subscribe(data=>{
  
     this.user4=data.results[id]
     console.log(this.user4)
    })
  }

}

