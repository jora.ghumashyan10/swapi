import { Component, Input, OnInit } from '@angular/core';
import { PlantsService } from '../plants.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-user2',
  templateUrl: './user2.component.html',
  styleUrls: ['./user2.component.css']
})
export class User2Component implements OnInit {

  product:any

  user2:any
  constructor(private plantsService:PlantsService, private activatedroute:ActivatedRoute,  private router: Router) {
  }
  ngOnInit(): void {
    this.getPlantsInfo()

  }

  getPlantsInfo(){
    let id=this.activatedroute.snapshot.params['id']
    this.plantsService.getPlants().subscribe(data=>{
  
     this.user2=data.results[id]
     console.log(this.user2)
    })
  }

}
