import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { VehiclesService } from '../vehicles.service';


@Component({
  selector: 'app-user5',
  templateUrl: './user5.component.html',
  styleUrls: ['./user5.component.css']
})
export class User5Component implements OnInit {

  product:any

  user5:any
  constructor(private vehiclesService:VehiclesService, private activatedroute:ActivatedRoute,  private router: Router) {
  }
  ngOnInit(): void {
    this.getVehiclesInfo()

  }

  getVehiclesInfo(){
    let id=this.activatedroute.snapshot.params['id']
    this.vehiclesService.getVehicles().subscribe(data=>{
  
     this.user5=data.results[id]
     console.log(this.user5)
    })
  }

}

