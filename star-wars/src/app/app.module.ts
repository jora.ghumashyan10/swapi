import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from "@angular/router";
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { PeopleComponent } from './people/people.component';
import { UserComponent } from './user/user.component';
import { PlantsComponent } from './plants/plants.component';
import { User2Component } from './user2/user2.component';
import { SpeciesComponent } from './species/species.component';
import { User3Component } from './user3/user3.component';
import { StarshipsComponent } from './starships/starships.component';
import { User4Component } from './user4/user4.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { User5Component } from './user5/user5.component';

@NgModule({
  declarations: [
    AppComponent,
    PeopleComponent,
    UserComponent,
    PlantsComponent,
    User2Component,
    SpeciesComponent,
    User3Component,
    StarshipsComponent,
    User4Component,
    VehiclesComponent,
    User5Component
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
