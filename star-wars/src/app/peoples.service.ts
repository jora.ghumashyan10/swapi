import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class PeoplesService {
  peoples: any;

  constructor(private http: HttpClient) {}

  public getPeoples(): Observable<readonly []> {
    return this.http.get<any>(`https://swapi.dev/api/people`);
  }

  public getPeoplesPage2(): Observable<any> {
    return this.http.get<any>(`https://swapi.dev/api/people/?page=2`);
  }
}
