import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class StarshipsService { //https://swapi.dev/api/planets/
  starships!:any
  constructor(private http:HttpClient) { }

  public getStarships():Observable<any> {
    return this.http.get<any>(`https://swapi.dev/api/starships`);
  }
}
